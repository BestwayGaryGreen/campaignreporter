﻿Imports System.Data.SqlClient
Imports System.IO

Public Class Form1

    Public fPath As String = "X:\Campaigns"

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim di As New DirectoryInfo(fPath)
        Dim folders As DirectoryInfo() = di.GetDirectories().OrderByDescending(Function(p) p.LastWriteTime).ToArray()

        Dim cnt As Int16 = 0

        For Each f As DirectoryInfo In folders
            ListBox1.Items.Add(f.Name)
            cnt += 1
            If cnt = 20 Then Exit For
        Next

        ListBox2.Items.Clear()

    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged

        ListBox2.Items.Clear()

        Dim fldr As String = ListBox1.SelectedItem.ToString()
        Dim cnt As Int16 = 0
        Dim list As New List(Of String)
        Dim asm As String = ""
        Dim csr As String = ""
        Dim route As String = ""
        Dim full As String = ""

        Dim d As DirectoryInfo = New DirectoryInfo(fPath + "\" + fldr)
        For Each f As FileInfo In d.GetFiles()

            route = Strings.Left(f.Name, 4).ToString().Replace("_", "")
            If route <> "Thum" Then     'thumbs.db
                asm = RunSQL(String.Format("SELECT v.ASMName FROM zeus.dbo.vwAllVSRsInfo v WHERE v.RouteNo = {0}", route))
                csr = RunSQL(String.Format("SELECT v.CSRName FROM zeus.dbo.vwAllVSRsInfo v WHERE v.RouteNo = {0}", route))
            End If
            full = String.Format("{0},{1},{2}", asm, route, csr)
            list.Add(full)
        Next

        Dim groups = list.GroupBy(Function(v) v)

        For Each g In groups
            ListBox2.Items.Add(g(0) + "," + g.Count.ToString())
        Next

    End Sub
    Private Sub Form1_SizeChanged(sender As Object, e As EventArgs) Handles Me.SizeChanged
        ListBox2.Width = Me.Width - ListBox2.Left - 30
        ListBox2.Height = Me.Height - ListBox2.Top - 50
        ListBox1.Height = ListBox2.Height
    End Sub

    Function RunSQL(ByVal sql As String, Optional ByVal db As String = "ivan") As String

        Dim conStr As String = "Data Source=MORBO;Initial Catalog=" + db + ";trusted_connection = true"
        Dim con As New SqlConnection(conStr)
        con.Open()
        Dim cmd As New SqlCommand(sql, con) With {
            .CommandTimeout = 180
        }
        Dim rdr As SqlDataReader = cmd.ExecuteReader

        Dim retval As String = ""

        While rdr.Read
            retval = rdr(0).ToString
        End While
        rdr.Close()
        con.Close()

        RunSQL = retval

    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        SaveFileDialog1.Filter = "CSV Files (*.csv*)|*.csv"
        If SaveFileDialog1.ShowDialog = DialogResult.OK Then

            Dim file As StreamWriter
            file = My.Computer.FileSystem.OpenTextFileWriter(SaveFileDialog1.FileName, True)

            For Each g In ListBox2.Items
                file.WriteLine(g)
            Next

            file.Close()
            MessageBox.Show("CSV File saved to " + SaveFileDialog1.FileName)
        End If

    End Sub
End Class
